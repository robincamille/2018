---
abstract: Learn how CPython implements the function call stack and how embedders can use this to extract useful reliability
  reports from their software!
duration: 30
level: Intermediate
presentation_url: https://slides.com/nikhilmarathe/diving-into-python-call-stacks
room: Madison
slot: 2018-10-05 13:30:00-04:00
speakers:
- Nikhil Marathe
title: A deep dive into Python stack frames
type: talk
---

This talk walks listeners through CPython's implementation of call stacks and how information is encoded in these stacks to aid execution and error recovery. Once we cover that, I will show how we modified the Crashpad crash reporter at Dropbox to extract these stacks from native crashes in the wild to make engineers lives easier. This allows our developers to detect crashes caused on _remote user machines_ due to incorrect Python code, with accurate Python stack traces.

The CPython interpreter implements function call stacks using various structures that act as chains of linked lists.
Each interpreter maintains a list of thread states for each thread. Each thread maintains a list of frames that represent the python function calls. New frames are pushed onto the stack as execution proceeds and the interpreter exposes these frames to user space. In addition, these frames can be inspected in native code to extract useful information about program crashes and to profile programs.

Some prior knowledge of C programming syntax, threading, [native function stacks](https://manybutfinite.com/post/journey-to-the-stack/) and [Python byte code](https://www.youtube.com/watch?v=cSSpnq362Bk) will be useful for attendees to get the most out of this talk.
