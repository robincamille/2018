---
abstract: New developers often ask, "How much Python do I need to know to get a job?".
  They overlook the other skills needed to be a successful software engineer. This
  talk highlights the other skill and tools needed, which can be used from the start
  of your coding journey.
duration: 30
level: Beginner
presentation_url:
room: PennTop South
slot: 2018-10-05 10:15:00-04:00
speakers:
- Kojo Idrissa
title: A Junior Developer's Guide to Software Engineering
type: talk
video_url: https://youtu.be/Ih_ios4SJNg
---

As people try to switch careers and become software engineers, they often focus *solely* on learning to program. However, there's more to being a professional developer or open source contributor than code. This talk highlights the what, when, and why of these other critical skills:

-  Version Control
-  Documentation
-  Testing & Test-Driven Development
-  Dependency Management & Deployment
-  Knowing Your Development Environment

This talk is for people trying to get started on the path to becoming a developer, as well as more experienced developers who're trying to guide or mentor those people.
