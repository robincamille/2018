---
abstract: "Many Python projects use Sphinx for documentation, so much so that Sphinx
  is like Python\u2019s secret weapon. It\u2019s a powerful, mature system for generating
  static content -- not just docs, but blogs and regular websites."
duration: 30
level: All
presentation_url:
room: PennTop South
slot: 2018-10-06 14:40:00-04:00
speakers:
- Paul Everitt
title: 'Customizing Sphinx: Simple, Normal, and Hard'
type: talk
video_url: https://youtu.be/7adnbsj9A4w
---

Many Python projects use Sphinx for documentation, so much so that Sphinx is like Python’s secret weapon. It’s a powerful, mature system for generating static content -- not just docs, but blogs and regular websites.

Sphinx can also be customized and extended in various ways, and here the trip gets wild and wooly. With such a venerable software stack, Sphinx can be inscrutable when you paint outside the lines.

This talk gives Sphinx civilians confidence to go beyond simply using Sphinx, towards adapting Sphinx to their needs: configuration values, local CSS and templates, installing extensions and themes, writing a small extension with a directive, and testing that extension. We’ll conclude with a roundup of challenges and benefits that come with diving into Sphinx.

As the talk provides shallow treatment of a wide topic list, follow-up and hands-on instruction will be given at an open space.
