---
abstract: Why does "a is b" evaluate to true if both variables are assigned to the
  integer 201 but not 498? When will Python still hold a reference to an object that
  has been deleted? How does Python's garbage collection differ from other languages
  like Ruby? Learn all that and more in this talk.
duration: 25
level: Intermediate
presentation_url:
room: PennTop North
slot: 2018-10-06 15:40:00-04:00
speakers:
- Theresa Lee
title: Memory Management in Python
type: talk
video_url: https://youtu.be/3pXquKQf2qk
---

Do you understand how Python works under the hood? Can you explain the following?

Why does `a is b` evaluate to `True` when both variables have a value of 201? Why does the statement evaluate to` False` if they both have a value of 498?
When will Python still hold a reference to an object that has been deleted?
How does Python collect garbage and is it any different from other languages like Ruby?

Learn the answers to these questions and better understand concepts like interning, reference counting, reference cycles, and generational garbage collection in this talk.
