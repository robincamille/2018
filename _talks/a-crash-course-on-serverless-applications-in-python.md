---
abstract: Heard about serverless computing but aren't sure how to get started? Curious
  about the benefits of serverless computing? In this talk I'll show you how to get
  up and running with serverless computing in less than 30 seconds.  Learn how to
  design and deploy serverless applications in python.
duration: 30
level: Beginner
presentation_url:
room: Madison
slot: 2018-10-05 10:45:00-04:00
speakers:
- James Saryerwinnie
title: A Crash Course on Serverless Applications in Python
type: talk
video_url: https://youtu.be/6Qj0XLsQe-0
---

There's a new class of applications that are becoming more prevalent these days: serverless applications. Serverless computing allows you to build and run applications without having to think about servers. Serverless apps don't require you to provision or scale any servers.

How can we take advantage of serverless computing as python developers? What tools and frameworks are available that make writing serverless apps a breeze? And most importantly, how do we design and structure our applications in this serverless world to ensure we're best utilizing the underlying services?

This is a practical talk on serverless architecture. In this talk, we'll look at several serverless architecture patterns covering real world use cases. We'll also look at actual open source serverless applications so we have concrete code examples we can review. These open source serverless applications leverage various AWS services including AWS Lambda and Amazon API Gateway. We'll also cover common strategies for deploying your serverless applications.

Come learn how to design serverless applications in python so you can focus on writing your core applications rather than worrying about managing servers.
