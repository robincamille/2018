---
name: MongoDB
tier: silver
site_url: https://www.mongodb.com
logo: mongodb.jpg
---
MongoDB is the leading modern, general purpose database platform, designed to unleash the power of
software and data for developers and the applications they build. Headquartered in New York, with
offices across North America, Europe, and Asia-Pacific, we are close to where you do business.
MongoDB has more than 4,900 customers in over 85 countries. The MongoDB database platform has been
downloaded over 30 million times and there have been more than 850,000 MongoDB University
registrations.
