---
name: Christine Zhang
talks:
- The Sum of Nothing
---

I'm a data journalist at [The Baltimore Sun](http://www.baltimoresun.com/bal-christine-zhang-20180802-staff.html), where I use data and statistics to tell news stories. Some of my loves include stats, stories, spreadsheets, and sandwiches ... and alliteration! I did a Knight-Mozilla OpenNews Fellowship at the Los Angeles Times Data Desk, where I was R user on a team of (primarily) Pythonistas.  During my last job at Two Sigma, I decided to challenge myself to get savvy with Python——or, in Harry Potter terms (I LOOOVE Harry Potter), learn some Parseltongue~~~

It's been a journey but I'm getting there, particularly with pandas, my love/hate Python package of choice.

https://twitter.com/christinezhang