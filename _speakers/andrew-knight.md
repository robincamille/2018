---
name: Andrew Knight
talks:
- Egad! How Do We Start Writing (Better) Tests?
---

Andy Knight is the "Automation Panda". He is a software engineer whose specialty is building test automation systems from the ground up, which involves both software development for test code as well as the infrastructure to run it in continuous integration. Currently, he is a Software Engineer in Test at PrecisionLender in Cary, NC. He also does independent consulting and training, and he is an avid Pythoneer. Check out his software blog at www.automationpanda.com.