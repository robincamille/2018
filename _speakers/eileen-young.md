---
name: Eileen Young
talks:
- Computer Modeling in the Social Science of Disasters
---

Graduate student currently pursuing a Master of Science in Disaster Science and Management at the University of Delaware, working as a research assistant on a project related to social aspects of agent-based modeling and evacuation. I’m passionate about issues of accessibility and social responsibility.