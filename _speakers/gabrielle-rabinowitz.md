---
name: Gabrielle Rabinowitz
talks:
- "Start \u2018em young: Tips for teaching Python to kids"
---

Gabrielle is the Lead Teacher for the BridgeUP: STEM program at the American Museum of Natural History where she teaches Python to high school girls and middle school students from around New York City. Gabrielle has taught in a variety of settings, from Bard College to Genspace, the world's first community biology lab. Her background is in molecular biology and she first learned Python in graduate school when her genomics datasets started crashing Excel. Now she hopes to help a new generation of scientists and data analysts grow up with Python in their toolbox from the start. Her work is focused on increasing access and support for women and other underrepresented minorities in computer science. 

Evan is an educator in the Youth Initiatives department at the American Museum of Natural History. He started his science journey at Stony Brook University, where he received a double major in astronomy and physics. He was then stuck at the crossroads of a PhD, and a career in education. Ultimately, his experience with the undergraduate astronomy club and all the outreach he did led him to pursue his masters in science education at NYU. From there, he spent five years as a high school physics teacher. The hardest thing he has ever had to do was to leave the public school system, but working at the museum has been the biggest re-affirming of his love for science.